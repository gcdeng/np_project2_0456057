#include <arpa/inet.h>		//For htonl(), htons().
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>			//For open().
#include <sys/types.h>		//For socket(), bind(), accept, semaphore, shared memory.
#include <sys/socket.h>		//For socket(), bind(), listen(), accept().
#include <sys/wait.h>		//For wait().
#include <sys/stat.h>		//For open().
#include <sys/time.h>		//For timeval.
#include <sys/ipc.h>		//For semaphore, shared memory.
#include <sys/sem.h>		//For semaphore.
#include <sys/shm.h>		//For shared memory.
#include <sys/errno.h>
#include <sys/select.h>		//For select().
#include <fcntl.h>			//For open().
#include <errno.h>			//For errno.
#include <stdlib.h>			//For exit(), setenv().
#include <unistd.h>			//For close(), read(), dup(), access().
#include <iostream>			//For cout.
#include <string.h>			//For bzero(), strspn(), strpbrk().
#include <signal.h>			//For signal().
#include <list>				//For list.
#include <ctype.h>

#define RL_BUFSIZE 15000
#define MAX_CMD_COUNT 256
#define MAX_CLIENT 30
#define MAX_PIPE_N 1000
#define MAX_PUBLIC_PIPE 100
#define MAX_MSG 1024

typedef struct client{
	char clientName[21]; //The length of a client's name is at most 20 characters.
	int clientID;
	int clientFD;
	char *clientIP;
	int clientPort;
	int pipeNtable[MAX_PIPE_N][3]; // pipeNtable[N][0]: counter, pipeNtable[N][1]: pipe_fd_in (4), pipeNtable[N][2]: pipe_fd_out (3)
	char *envName;
	char *envConfig;
} Client;

typedef struct command{
	int pipeN;
	int errPipeN;
	int publicPipeIn;
	int publicPipeOut;
	int writeFile;
	char cmdBuf[RL_BUFSIZE];
	char fileName[20];
	char writePublicPipe[20];
	char readPublicPipe[20];
	int exitCMD;
} Command;

// socket
int passiveTCP(char *, int);
int passivesock(char *, char *, int);
// client
Client initialize_client(int, int, int, char *);
void initialize_clientHandler(Client(&)[MAX_CLIENT]);
// command
void initialize_command(Command *);
char *read_line(int, Command *);
void split_execute(char *, Client *, Client(&)[MAX_CLIENT], Command *);
int execute(char **, Client *, Client(&)[MAX_CLIENT], Command *, int);
void printenvHandler(Client *, char **);
void setenvHandler(Client *);
void unknownCMD(int, char *);
// Add more built-in commands
int broadcastHandler(Client *, Client(&)[MAX_CLIENT], char *, int);
void whoHandler(Client *, Client(&)[MAX_CLIENT]);
void nameHandler(Client *, Client(&)[MAX_CLIENT], char *);
void tellHandler(Client *, Client(&)[MAX_CLIENT], int, char *);
// pipe
int build_pipeNtable(int pipeNtable[MAX_PIPE_N][3], int N);
void decrease_pipeNtable(Client *, int);
void check_pipeNtable(Client *);
// other
void welcome(int);
void err_dump(const char *);

bool publicPipe[MAX_PUBLIC_PIPE];

int main(int argc, char *argv[])
{
	chdir("./ras");

	int msock; //master server socket
	char *service = "echo"; //service name or port number
	struct sockaddr_in fsin; //the from address of a client
	fd_set rfds;  // read file descriptor set
	fd_set afds;  // active file descriptor set
	int fd, nfds;
	int alen; //from address length
	
	int clientIndex;
	char *line;
	Client clientHandler[MAX_CLIENT];
	initialize_clientHandler(clientHandler); // initialize every client
	Command cmd;
	initialize_command(&cmd); // initialize command variables

	switch (argc) {
	case 1:
		break;
	case 2:
		service = argv[1];
		break;
	default:
		err_dump("usage: ./server [port]");
	}

	msock = passiveTCP(service, MAX_CLIENT);
	nfds = msock+1; //getdtablesize()
	FD_ZERO(&afds);
	FD_SET(msock, &afds);

	while(1){
		memcpy(&rfds, &afds, sizeof(rfds));
		if(select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (struct timeval *)0) < 0)
			err_dump("server select() error");

		if (FD_ISSET(msock, &rfds)) {
			int ssock;
			alen = sizeof(fsin);
			// accept the socket
			ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t*)&alen);
			if (ssock < 0) //accept error
				err_dump("server accept() error");
			else{
				if (ssock > (nfds - 1))
					nfds = ssock + 1;

				for(clientIndex = 0; clientIndex < MAX_CLIENT; clientIndex++){
					if (clientHandler[clientIndex].clientID == -1){
						FD_SET(ssock, &afds);
						// set client config: ID, FD, Port, IP
						clientHandler[clientIndex] = initialize_client(clientIndex, ssock, fsin.sin_port, inet_ntoa(fsin.sin_addr));
						// preset environment
						setenvHandler(&clientHandler[clientIndex]);
						// welcome message
						welcome(ssock);
						// new client enter: no mane user
						broadcastHandler(&clientHandler[clientIndex], clientHandler, NULL, 0);
						write(ssock, "% ", 2);
						break;
					}
				}

				// if more than 30 clients
				if (clientIndex == MAX_CLIENT){
					write(ssock, "Connection failed: Server has reached its' maximum connection ammount. Please try again later.\n",
						strlen("Connection failed: Server has reached its' maximum connection ammount. Please try again later.\n"));
					close(ssock);
					clientIndex = 0;
				}
			}
		}

		for (fd = 0; fd < nfds; fd++){
			if (fd != msock && FD_ISSET(fd, &rfds)){
				for(clientIndex = 0; clientIndex < MAX_CLIENT; clientIndex++){
					if (fd == clientHandler[clientIndex].clientFD){
						setenvHandler(&clientHandler[clientIndex]);
						line = read_line(clientHandler[clientIndex].clientFD, &cmd);
						printf("read line: %s, length: %d\n", line, strlen(line));
						split_execute(line, &clientHandler[clientIndex], clientHandler, &cmd);

						if(cmd.exitCMD == 1){
							// initialize after exit command
							broadcastHandler(&clientHandler[clientIndex], clientHandler, NULL, 5); //client left
							close(clientHandler[clientIndex].clientFD);
							FD_CLR(clientHandler[clientIndex].clientFD, &afds);
							clientHandler[clientIndex] = initialize_client(-1, -1, -1, NULL);
							initialize_command(&cmd);
						}else{
							write(clientHandler[clientIndex].clientFD, "% ", strlen("% "));
						}

						break;
					}
				}
				if (clientIndex == MAX_CLIENT)
					write(msock, "Server: client does not exist", strlen("Server: client does not exist"));
			}
		}
	}
}

Client initialize_client(int id, int sockfd, int port, char *ip){
	Client newClient;
	newClient.clientID = id;
	newClient.clientFD = sockfd;
	newClient.clientPort = port;
	newClient.clientIP = ip;
	strcpy(newClient.clientName, "(no name)\0");
	for (int i = 0; i < MAX_PIPE_N; i++){
		newClient.pipeNtable[i][0] = -1;
	}
	newClient.envName = "PATH";
	newClient.envConfig = "bin:.";
	return newClient;
}

void initialize_clientHandler(Client(&clientHandler)[MAX_CLIENT]){
	for (int i = 0; i < MAX_CLIENT; i++)
		clientHandler[i] = initialize_client(-1, -1, -1, NULL);
}

void initialize_command(Command *cmd){
	cmd->pipeN = 0;
	cmd->errPipeN = 0;
	cmd->exitCMD = 0;
	cmd->writeFile = 0;
	strcpy(cmd->fileName,"");
	cmd->publicPipeOut = 0;
	strcpy(cmd->writePublicPipe,"");
	cmd->publicPipeIn = 0;
	strcpy(cmd->readPublicPipe,"");
	// strcpy(cmd->cmdBuf,"");
}

/*reading a line*/
char *read_line(int newfd, Command *cmd){
  int n = -1;
  int end = 0;
  char buffer[RL_BUFSIZE];
  char *inputCommand = new char[RL_BUFSIZE];
  bzero(inputCommand, RL_BUFSIZE);
  bzero(cmd->cmdBuf, RL_BUFSIZE);
  strcpy(cmd->cmdBuf, "");
  while(!end){
    bzero(buffer, RL_BUFSIZE);
    if ((n = read(newfd, buffer, RL_BUFSIZE)) < 0){
      printf("reading from socket...\n");
    } else {
      strcat(inputCommand, buffer);
      strcat(cmd->cmdBuf, buffer);
      if(buffer[n-1] == '\n'){
        end = 1;
      }
    }
  }
  strcpy(cmd->cmdBuf, strtok(cmd->cmdBuf, "\r\n"));
  return inputCommand;
}

// split command line then execute
void split_execute(char *line, Client *cli, Client(&clientHandler)[MAX_CLIENT], Command *cmd){
	initialize_command(cmd);
	printf("line: %s, cmd->cmdBuf: %s\n", line, cmd->cmdBuf);
	char *delim = " \r\n";
	char *executeCMD[MAX_CMD_COUNT];
	char **tmp = executeCMD;
	char msg[MAX_MSG];
	int exeStatus = 0;
	int tag = 1;
	int pubPipeError = 0;
	char *token = strtok(line, delim);
	while(1){
		printf("token: %s\n", token);

		if (token == NULL){
			if (pubPipeError){
				if (!tag) {
					check_pipeNtable(cli);
					decrease_pipeNtable(cli, 0);
				}
				return;
			}
			*tmp = NULL;
			if (execute(executeCMD, cli, clientHandler, cmd, tag) != -1 || !tag){
				decrease_pipeNtable(cli, 0);
			}
			return;
		}
		if (strcmp(token, "yell") == 0){
			char yellMsg[300] = "";
			strcpy(yellMsg, cmd->cmdBuf+(strspn(cmd->cmdBuf, "yell")+1));
			broadcastHandler(cli, clientHandler, yellMsg, 2);
			return;
		}
		if (strcmp(token, "tell") == 0){
			token = strtok(NULL, delim);
			int reciever = atoi(token) - 1;

			char tellMsg[MAX_MSG] = "";
			if (reciever < 9){ //tell ID is <10, only one digit
				strcpy(tellMsg, cmd->cmdBuf+7);
			} else { // tell ID is >=10, have two digit
				strcpy(tellMsg, cmd->cmdBuf+8);
			}
			tellHandler(cli, clientHandler, reciever, tellMsg);
			return;
		}
		if (strcmp(token, "|") == 0){ //general pipe
			cmd->pipeN = -1;
			exeStatus = 1;
			token = strtok(NULL, delim); //go to next token begin last token
			continue;
		}
		if (token[0] == '|' && token[1] != '\0'){ //have pipe number |N
			cmd->pipeN = atoi(token+1); //get pipe number
			exeStatus = 1;
			token = strtok(NULL, delim);
			continue;
		}
		if (token[0] == '!'){
			cmd->errPipeN = atoi(token+1); //get error pipe number
			exeStatus = 1;
			token = strtok(NULL, delim);
			continue;
		}
		if (strcmp(token, ">") == 0){
			cmd->writeFile = 1;
			exeStatus = 1;
			token = strtok(NULL, delim);
			strcpy(cmd->fileName, token); //get file name
			token = strtok(NULL, delim);
			continue;
		}
		if (token[0] == '>'){ //public pipe out
			if ( publicPipe[atoi(token+1) - 1] ){
				sprintf(msg, "*** Error: public pipe #%d already exists. ***\n", atoi(token+1));
				write(cli->clientFD, msg, strlen(msg));
				pubPipeError = 1;
				token = strtok(NULL, delim);
				continue;
			} else {
				publicPipe[atoi(token+1) - 1] = 1;
				cmd->publicPipeOut = 1;
				exeStatus = 1;
				sprintf(cmd->writePublicPipe, "../%s.txt", token+1);
				token = strtok(NULL, delim);
				continue;
			}
		}
		if (token[0] == '<'){ //public pipe in
			if (publicPipe[atoi(token+1) - 1] == 0){
				sprintf(msg, "*** Error: public pipe #%d does not exist yet. ***\n", atoi(token+1));
				write(cli->clientFD, msg, strlen(msg));
				pubPipeError = 1;
				token = strtok(NULL, delim);
				continue;
			} else {
				publicPipe[atoi(token+1) - 1] = 0;
				cmd->publicPipeIn = 1;
				// exeStatus = 1;
				sprintf(cmd->readPublicPipe, "../%s.txt", token+1);
				token = strtok(NULL, delim);
				continue;	
			}
		}
		if (exeStatus == 1){
			if (pubPipeError){
				if (!tag) {
					check_pipeNtable(cli);
					decrease_pipeNtable(cli, 0);
				}
				return;
			}
			*tmp = NULL;
			if (execute(executeCMD, cli, clientHandler, cmd, tag) == -1){
				if (tag == 0){
					decrease_pipeNtable(cli, 0);
				}
				return;
			}
			// initialize
			decrease_pipeNtable(cli, 1);
			initialize_command(cmd);
			tag = 0;
			exeStatus = 0;
			tmp = executeCMD;
			strcpy(msg, "");
		}
		*(tmp++) = token;
		token = strtok(NULL, delim);
	}
}

/***
* execute command
* return 0 is continue to do
* return 1 is exit
* return -1 is empty command
*/
int execute(char **executeCMD, Client *cli, Client(&clientHandler)[MAX_CLIENT] ,Command *cmd, int tag){
	if (executeCMD[0] == NULL){
		return -1;
	}
	if (strcmp(executeCMD[0], "exit") == 0){
		cmd->exitCMD = 1;
		check_pipeNtable(cli);
		return 1;
	}
	if (strcmp(executeCMD[0], "printenv") == 0){
		printenvHandler(cli, executeCMD);
		check_pipeNtable(cli);
		return 0;
	}
	if (strcmp(executeCMD[0], "setenv") == 0){
		cli->envName = executeCMD[1];
		cli->envConfig =  executeCMD[2];
		setenvHandler(cli);
		check_pipeNtable(cli);
		return 0;
	}

	if (strcmp(executeCMD[0], "who") == 0){
		whoHandler(cli, clientHandler);
		check_pipeNtable(cli);
		return 0;	
	}

	if (strcmp(executeCMD[0], "name") == 0){
		nameHandler(cli, clientHandler, executeCMD[1]);
		check_pipeNtable(cli);
		return 0;
	}

	int pipeIndex = 0;
	int errPipeIndex = 0;
	if (cmd->pipeN != 0)
		pipeIndex = build_pipeNtable(cli->pipeNtable, cmd->pipeN);
	if (cmd->errPipeN > 0)
		errPipeIndex = build_pipeNtable(cli->pipeNtable, cmd->errPipeN);

	int childpid;
	if ((childpid = fork()) < 0){
		// Error forking
    	printf("[execute error forking]\n");
      	exit(EXIT_FAILURE);
	} else if (childpid == 0){
		// child process
		for (int i = 0; i < MAX_PIPE_N; i++){ //check if pipe number is last one
			if (cli->pipeNtable[i][0] == 0){
				dup2(cli->pipeNtable[i][2]/10000, STDIN_FILENO); //pipeout pipe to stdin
				break;
			}
		}

		if (cmd->pipeN != 0){
			dup2(abs(pipeIndex)%10000, STDOUT_FILENO); //stdout pipe to pipein
		} else {
			dup2(cli->clientFD, STDOUT_FILENO); //socket stdin
		}

		if (cmd->errPipeN > 0){
			dup2(abs(errPipeIndex)%10000, STDERR_FILENO); //stderr pipe to errpipe
		} else {
			dup2(cli->clientFD, STDERR_FILENO); //socket stderr
		}

		// > write file
		if (cmd->writeFile){
			FILE *fPtr;
			fPtr = freopen(cmd->fileName, "w", stdout);
			if(!fPtr){
	          printf("failed to write file\n");
	          exit(EXIT_FAILURE);
	        }
		}

		// public pipe in 
		if (cmd->publicPipeIn){
			FILE *ppr;
			ppr = freopen(cmd->readPublicPipe, "r", stdin);
			if(!ppr){
	          printf("failed to read public pipe file\n");
	          exit(EXIT_FAILURE);
	        } else {
	        	remove(cmd->readPublicPipe); //remove public pipe file
	        }
		}

		// public pipe out
		if (cmd->publicPipeOut){
			FILE *ppw;
			ppw = freopen(cmd->writePublicPipe, "w", stdout);
			if(!ppw){
	          printf("failed to write public pipe file\n");
	          exit(EXIT_FAILURE);
	        }
		}

		if (execvp(executeCMD[0], executeCMD) == -1){
			unknownCMD(cli->clientFD, executeCMD[0]);
			cmd->publicPipeOut = 0;
			cmd->publicPipeIn = 0;
			exit(EXIT_FAILURE);
		}
	} else {
		// parent process
		int status;
		printf("wait pid: %d\n", wait(&status));
		if (status != 256) {
			if (cmd->publicPipeIn){
				broadcastHandler(cli, clientHandler, cmd->cmdBuf, 3);
			}
			if (cmd->publicPipeOut){
				broadcastHandler(cli, clientHandler, cmd->cmdBuf, 4);
			}
		}
		if (status != 256 || !tag)
	    	check_pipeNtable(cli);

	    if (status == 256) {
	    	if (pipeIndex > 0){
	    		for (int i = 0; i < MAX_PIPE_N; i++){
	    			if (cli->pipeNtable[i][0] == pipeIndex){
	    				close(cli->pipeNtable[i][2] % 10000);
	    				close(cli->pipeNtable[i][2] / 10000);
	    				cli->pipeNtable[i][0] = -1;
	    				break;
	    			}
	    		}
	    	}
	    	if (errPipeIndex > 0){
	    		for (int j = 0; j < MAX_PIPE_N; j++){
	    			if (cli->pipeNtable[j][0] == errPipeIndex){
	    				close(cli->pipeNtable[j][2] % 10000);
	    				close(cli->pipeNtable[j][2] / 10000);
	    				cli->pipeNtable[j][0] = -1;
	    				break;
	    			}
	    		}
	    	}
	    	return -1;
	    }
	}
	return 0;
}

/***
* config  act message
* 0       new client
* 1       named
* 2       yelled
* 3       received public pipe
* 4       piped to
* 5       client left
*/
int broadcastHandler(Client *client, Client(&clientHandler)[MAX_CLIENT], char *broadcastMsg, int config){
	if(client->clientID != -1){
		char msg[300] = "";
		switch(config){
			case 0:
				// new client: no name
				sprintf(msg, "*** User '%s' entered from CGILAB/511. ***\n", client->clientName);
				break;
			case 1:
				// named
				sprintf(msg, "*** User from CGILAB/511 is named '%s'. ***\n", client->clientName);
				break;
			case 2:
				// yelled
				sprintf(msg, "*** %s yelled ***: %s\n",
					client->clientName, broadcastMsg);
				break;
			case 3:
				// received public pipe
				sprintf(msg, "*** %s (#%d) just received via '%s' ***\n",
					client->clientName, client->clientID+1, broadcastMsg);
				break;
			case 4:
				// piped to
				sprintf(msg, "*** %s (#%d) just piped '%s' ***\n",
					client->clientName, client->clientID+1, broadcastMsg);
				break;
			case 5:
				// client left
				sprintf(msg, "*** User '%s' left. ***\n", client->clientName);
				break;
		}

		for (int i = 0; i < MAX_CLIENT; i++){
			if (clientHandler[i].clientID != -1){
				write(clientHandler[i].clientFD, msg, strlen(msg));
			}
		}
		return 1;
	}else{
		return 0;
	}
}

void welcome(int sockfd){
	//Print welcome message.
	write(sockfd,"****************************************\n** Welcome to the information server. **\n****************************************\n",
		strlen("****************************************\n** Welcome to the information server. **\n****************************************\n"));
}

int passiveTCP(char *service, int qlen){
	printf("[passiveTCP]\n");
	char protocol[10] = "tcp";
	return passivesock(service, protocol, qlen);
}

int passivesock(char *service, char *protocol, int qlen){
	struct servent *pse;												//Pointer to service information entry.
	struct protoent *ppe;												//Pointer to protocol information entry.
	struct sockaddr_in Sin;												//An Internet endpoint address.
	int s, type;														//Socket descriptor and socket type.
	u_short portbase = 0;

	bzero((char *)&Sin, sizeof(Sin));
	Sin.sin_family = AF_INET;
	Sin.sin_addr.s_addr = INADDR_ANY;

	//Map service name to port number.
	if (pse = getservbyname(service, protocol))
		Sin.sin_port = htons(ntohs((u_short)pse->s_port) + portbase);
	else if ((Sin.sin_port = htons((u_short)atoi(service))) == 0)
		printf("passivesock: can't get service entry\n");

	//Map protocol name to protocol number.
	if ((ppe = getprotobyname(protocol)) == 0)
		printf("passivesock: can't get protocol entry\n");

	//Use protocol to choose a socket type.
	if (strcmp(protocol, "udp") == 0)
		type = SOCK_DGRAM;
	else
		type = SOCK_STREAM;

	//Allocate a socket.
	s = socket(PF_INET, type, ppe->p_proto);
	if (s < 0)
		printf("passivesock: can't create socket\n");

	//Bind the socket.
	if (bind(s, (struct sockaddr *)&Sin, sizeof(Sin)) < 0)
		printf("passivesock: can't bind to port\n");

	//Listen port
	if (type == SOCK_STREAM && listen(s, qlen) < 0)
		printf("passivesock: can't listen on port\n");
	return s;
}

int build_pipeNtable(int pipeNtable[MAX_PIPE_N][3], int N){
	// int pipeIndex = 0;
	int i;
	int pipe_fd[2];

	if (N > 0){
		for (int j = 0; j < MAX_PIPE_N; j++){
			if (pipeNtable[j][0] == N){
				return -1 * pipeNtable[j][2];
			}
		}
	}
	
    if (pipe(pipe_fd) == -1){
      	printf("Error: Unable to create pipe.\n");
      	exit(EXIT_FAILURE);
    }

    for (i = 0; i < MAX_PIPE_N; i++){
		if (pipeNtable[i][0] == -1){
			// counter
			pipeNtable[i][0] = abs(N); //pipe number
			// tag for number pipe
			if (N>0) {
				pipeNtable[i][1] = 1; //have |N
			} else {
				pipeNtable[i][1] = 0; //general pipe
			}
			pipeNtable[i][2] = 10000 * pipe_fd[0] + pipe_fd[1]; //pipe in
			// pipeNtable[i][2] = pipe_fd[0]; //pipe out
			// pipeIndex = i;
			break;
		}
    }
    printf("pipeNtable[i][0]:%d, pipeNtable[i][2]:%d\n", pipeNtable[i][0], pipeNtable[i][2]);
    return pipeNtable[i][2];
}

// decrease number-pipe counter
void decrease_pipeNtable(Client *cli, int N){
  if(N == 1){
    for (int i = 0; i < MAX_PIPE_N; i++){
      if( cli->pipeNtable[i][0] == 1 && !cli->pipeNtable[i][1])
        cli->pipeNtable[i][0]--;
    }
  } else {
    for (int j = 0; j < MAX_PIPE_N; ++j){
      if( cli->pipeNtable[j][0] > 0 ){
        cli->pipeNtable[j][0]--;
      }
    }
  }
}

// check table when back to parent process
void check_pipeNtable(Client *cli){
  for (int i = 0; i < MAX_PIPE_N; ++i){
    if(cli->pipeNtable[i][0] == -1)
      continue;

    if(cli->pipeNtable[i][0] == 1)
      close(cli->pipeNtable[i][2] % 10000); //close pipe fd in

    if(cli->pipeNtable[i][0] == 0){
      close(cli->pipeNtable[i][2] / 10000); //close pipe fd out
      cli->pipeNtable[i][0] = -1; //initialize
    }
  }
}

void setenvHandler(Client *cli){
	setenv(cli->envName, cli->envConfig, 1);
}

void printenvHandler(Client *cli, char **executeCMD){
	write(cli->clientFD, executeCMD[1], strlen(executeCMD[1]));
	write(cli->clientFD, "=", 1);
	write(cli->clientFD, getenv(executeCMD[1]), strlen(getenv(executeCMD[1])));
	write(cli->clientFD, "\n", 1);
}

void unknownCMD(int sockfd, char *CMD){
	write(sockfd, "Unknown command: [", strlen("Unknown command: ["));
	write(sockfd, CMD, strlen(CMD));
	write(sockfd, "].\n", strlen("].\n"));
}

void whoHandler(Client *cli, Client(&clientHandler)[MAX_CLIENT]){
	write(cli->clientFD, "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n", strlen("<ID>\t<nickname>\t<IP/port>\t<indicate me>\n"));
	for (int i = 0; i < MAX_CLIENT; i++){
		if(clientHandler[i].clientID != -1){
			char msg[100] = "";
			sprintf(msg, "%d\t%s\tCGILAB/511", i+1, clientHandler[i].clientName);
			write(cli->clientFD, msg, strlen(msg));
			if (cli->clientFD == clientHandler[i].clientFD){
				write(cli->clientFD, "\t<-me\n", strlen("\t<-me\n"));
			} else {
				write(cli->clientFD, "\n", strlen("\n"));
			}
		}
	}
}

void nameHandler(Client *cli, Client(&clientHandler)[MAX_CLIENT], char *name){
	// check exists
	for (int i = 0; i < MAX_CLIENT; i++){
		if (clientHandler[i].clientID != -1){
			if (strcmp(name, clientHandler[i].clientName) == 0){
				char msg[100] = "";
				sprintf(msg, "*** User '%s' already exists. ***\n", name);
				write(cli->clientFD, msg, strlen(msg));
				return;
			}
		}
	}

	if (strlen(name) > 20){ //check if name more than 20 characters
		strncpy(cli->clientName, name, 20);
	} else {
		strcpy(cli->clientName, name);
	}

	broadcastHandler(cli, clientHandler, NULL, 1);
}

void tellHandler(Client *cli, Client(&clientHandler)[MAX_CLIENT], int reciever, char *tellMsg){
	char msg[300] = "";

	if (reciever > -1 && reciever < 30){
		if (clientHandler[reciever].clientID != -1){
			sprintf(msg, "*** %s told you ***: %s\n", cli->clientName, tellMsg);
			write(clientHandler[reciever].clientFD, msg, strlen(msg));
		} else {
			sprintf(msg, "*** Error: user #%d does not exist yet. ***\n", (reciever + 1));
			write(cli->clientFD, msg, strlen(msg));
		}
	} else {
		write(cli->clientFD, "Error: Client Id must between 1~30.\n", strlen("Error: Client Id must between 1~30.\n"));
	}
}

void err_dump(const char *msg){
	perror(msg);
	exit(EXIT_FAILURE);
}