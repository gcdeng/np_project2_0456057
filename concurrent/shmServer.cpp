#include <arpa/inet.h>		//For htonl(), htons().
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>			//For open().
#include <sys/types.h>		//For socket(), bind(), accept, semaphore, shared memory.
#include <sys/socket.h>		//For socket(), bind(), listen(), accept().
#include <sys/wait.h>		//For wait().
#include <sys/stat.h>		//For open().
#include <sys/time.h>		//For timeval.
#include <sys/ipc.h>		//For semaphore, shared memory.
#include <sys/sem.h>		//For semaphore.
#include <sys/shm.h>		//For shared memory.
#include <sys/errno.h>
#include <sys/select.h>		//For select().
#include <fcntl.h>			//For open().
#include <errno.h>			//For errno.
#include <stdlib.h>			//For exit(), setenv().
#include <unistd.h>			//For close(), read(), dup(), access().
#include <iostream>			//For cout.
#include <string.h>			//For bzero(), strspn(), strpbrk().
#include <signal.h>			//For signal().
#include <list>				//For list.
#include <ctype.h>

#define RL_BUFSIZE 15000
#define MAX_CMD_COUNT 256
#define MAX_CLIENT 30
#define MAX_PIPE_N 1000
#define MAX_PUBLIC_PIPE 100
#define MAX_MSG 1024
#define MAX_URMSG 10
// semaphores key
#define SEMKEY1 52364
#define SEMKEY2 45612
// shared memory key
#define SHMKEY1 13579
#define SHMKEY2 24680
#define PERMS 0666

static struct sembuf op_lock[2] = {
	0, 0, 0,
	0, 1, SEM_UNDO
};

static struct sembuf op_unlock[1] = {
	0, -1, (IPC_NOWAIT | SEM_UNDO)
};

typedef struct shmClient{
	char clientName[21]; //The length of a client's name is at most 20 characters.
	int clientID;
	int clientPID;
	int clientFD;
	int clientPort;
	char *clientIP;
	char chatBuf[MAX_URMSG][MAX_MSG];
} ShmClient;

typedef struct privateClient{
	int clientID;
	int clientFD;
	int pipeNtable[MAX_PIPE_N][3];
} PrvClient;

typedef struct command{
	int pipeN;
	int errPipeN;
	int publicPipeIn;
	int publicPipeOut;
	int writeFile;
	char fileName[20];
	char writePublicPipe[10];
	char readPublicPipe[10];
	char cmdBuf[RL_BUFSIZE];
	int exitCMD;
} Command;


// socket
int passiveTCP(void);
int passivesock(char *, char *, int);
// client
void initialize_shmClient();
void initialize_prvClient(PrvClient *, int);
int create_client(int, char *, int);
// command
void initialize_command(Command *);
char *read_line(int, Command *);
void split_execute(char *, Command *, PrvClient *);
int execute(char **, PrvClient *, Command *, int);
void printenvHandler(PrvClient *, char **);
void unknownCMD(int, char *);
// Add more built-in commands
void whoHandler(PrvClient *);
void nameHandler(PrvClient *, char *);
void tellHandler(PrvClient *, int, char *);
int broadcastHandler(PrvClient *, char *, int);
void write_message(int);
// pipe
void initialize_publicPipeIndex();
int build_pipeNtable(int pipeNtable[MAX_PIPE_N][3], int N);
void decrease_pipeNtable(PrvClient *, int);
void check_pipeNtable(PrvClient *);
void clean_pipeNtable(PrvClient *);
// lock
void my_lock(int);
void my_unlock(int);
void remove_sharedmemory(int);
// other
void welcome(int);
void err_dump(const char *);

// semaphores var
int semIdForClient;
int semIdForPubPipe;
// shared memory var: share client data and public pipe data
int shmClientID;
ShmClient *shmClient;
int pubPipeID;
int *publicPipeIndex;

int main(int argc, char *argv[])
{

	chdir("./ras");

	/***
	* Setting shared memory and semaphores
	*/
	// get semaphores ID
	if ((semIdForClient = semget(SEMKEY1, 1, PERMS | IPC_CREAT)) < 0)
		exit(1);
	if ((semIdForPubPipe = semget(SEMKEY2, 1, PERMS | IPC_CREAT)) < 0)
		exit(1);
	// get shared memory ID
	if ((shmClientID = shmget(SHMKEY1, (sizeof(ShmClient)*MAX_CLIENT), PERMS | IPC_CREAT)) < 0){ //30
		exit(1);
	}
	if ((pubPipeID = shmget(SHMKEY2, (sizeof(int)*MAX_PUBLIC_PIPE), PERMS | IPC_CREAT)) < 0){ //100
		exit(1);
	}
	// attach the shared memory segment
	if ((shmClient = (ShmClient *)shmat(shmClientID, NULL, 0)) == (ShmClient *)-1) {
		exit(1);
	}
	if ((publicPipeIndex = (int *)shmat(pubPipeID, NULL, 0)) == (int *)-1) {
		exit(1);
	}
	initialize_shmClient();
	initialize_publicPipeIndex();
	
	// socket
	int msock; //master server socket
	int ssock; //slave server socket
	// char *service = "5544";
	msock = passiveTCP();
	struct sockaddr_in fsin; //the from address of a client
	socklen_t alen; //length of client's address
	alen = sizeof(fsin);
	
	// remove shared memory
	signal(SIGINT, remove_sharedmemory);

	while(ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t*)&alen)){
		int childpid;
		childpid = fork();
		if (childpid < 0){
			// Error forking
	        printf("[server forking error]\n");
	        exit(EXIT_FAILURE);
		} else if (childpid == 0){
			// child
			printf("[children process]\n");

			close(msock);
			setenv("PATH", "bin:.", 1);
			char *line;
			// preset command
			Command cmd;
			initialize_command(&cmd);

			// preset private client
			PrvClient prvCli;
			initialize_prvClient(&prvCli, ssock);
			prvCli.clientID = create_client(ssock, inet_ntoa(fsin.sin_addr), fsin.sin_port);
			
			welcome(ssock);
			signal(SIGUSR1, write_message);
			// new no name client broadcast
			broadcastHandler(&prvCli, NULL, 0);
			
			do {
				write(ssock, "% ", strlen("% "));
				line = read_line(ssock, &cmd);
				split_execute(line, &cmd, &prvCli);
			} while(!cmd.exitCMD);

			// exit broadcast
			broadcastHandler(&prvCli, NULL, 5);
			printf("[exit]\n");

			// initialize
			my_lock(semIdForClient);
			shmClient[prvCli.clientID].clientID = -1;
			my_unlock(semIdForClient);
			clean_pipeNtable(&prvCli);
			initialize_command(&cmd);
			
			// detach the shared memory segment
			if(shmdt(shmClient) <0)
				printf("[shmClient detach error.]\n");
			if(shmdt(publicPipeIndex) <0 )
				printf("[publicPipeIndex detach error.]\n");

			close(ssock);
			exit(1);
		}
		// parent
		printf("[parent process]\n");
		close(ssock);
		// break;
	}
	return 0;
}

void initialize_shmClient(){
	my_lock(semIdForClient);
	for (int i = 0; i < MAX_CLIENT; i++)
	{
		strcpy(shmClient[i].clientName, "");
		shmClient[i].clientID = -1;
		shmClient[i].clientPID = -1;
		shmClient[i].clientFD = -1;
		shmClient[i].clientPort = -1;
		shmClient[i].clientIP = NULL;
		for (int j = 0; j < MAX_URMSG; j++)
			strcpy(shmClient[i].chatBuf[j], "");
	}
	my_unlock(semIdForClient);
}

void initialize_prvClient(PrvClient *prvCli, int fd){
	prvCli->clientID = -1;
	prvCli->clientFD = fd;
	for (int i = 0; i < MAX_PIPE_N; i++)
	{
		prvCli->pipeNtable[i][0] = -1;
	}
}

void initialize_command(Command *cmd){
	cmd->pipeN = 0;
	cmd->errPipeN = 0;
	cmd->exitCMD = 0;
	cmd->writeFile = 0;
	strcpy(cmd->fileName,"");
	cmd->publicPipeOut = 0;
	strcpy(cmd->writePublicPipe,"");
	cmd->publicPipeIn = 0;
	strcpy(cmd->readPublicPipe,"");
	// strcpy(cmd->cmdBuf,"");
}

void initialize_publicPipeIndex(){
	my_lock(semIdForPubPipe);
	for (int i = 0; i < MAX_PUBLIC_PIPE; i++)
	{
		publicPipeIndex[i] = 0;
	}
	my_unlock(semIdForPubPipe);
}

// create a new client, return client ID for prvCli
int create_client(int fd, char *ip, int port){
	int id;
	my_lock(semIdForClient);
	for (id = 0; id < MAX_CLIENT; id++){
		if (shmClient[id].clientID == -1){
			shmClient[id].clientID = id;
			break;
		}
	}
	if (id == MAX_CLIENT)
		exit(1);

	shmClient[id].clientPID = getpid();
	shmClient[id].clientFD = fd;
	shmClient[id].clientIP = ip;
	shmClient[id].clientPort = port;
	strcpy(shmClient[id].clientName, "(no name)\0");
	for (int j = 0; j < MAX_URMSG; j++)
		strcpy(shmClient[id].chatBuf[j], "");
	my_unlock(semIdForClient);
	return id;
}

/*reading a line*/
char *read_line(int fd, Command *cmd){
  int n = -1;
  int end = 0;
  char buffer[RL_BUFSIZE];
  char *inputCommand = new char[RL_BUFSIZE];
  bzero(inputCommand, RL_BUFSIZE);
  bzero(cmd->cmdBuf, RL_BUFSIZE);
  strcpy(cmd->cmdBuf, "");
  while(!end){
    bzero(buffer, RL_BUFSIZE);
    if ((n = read(fd, buffer, RL_BUFSIZE)) < 0){
      printf("[reading from socket...]\n");
    } else {
		strcat(inputCommand, buffer);
		strcat(cmd->cmdBuf, buffer);
		if(buffer[n-1] == '\n')
			end = 1;
    }
  }
  strcpy(cmd->cmdBuf, strtok(cmd->cmdBuf, "\r\n"));
  return inputCommand;
}

// split command line then execute
void split_execute(char *line, Command *cmd, PrvClient *cli){
	initialize_command(cmd);
	printf("line: %s, cmd->cmdBuf: %s\n", line, cmd->cmdBuf);

	char *delim = " \r\n";
	char *executeCMD[MAX_CMD_COUNT];
	char **tmp = executeCMD;
	char msg[MAX_MSG];
	int exeStatus = 0;
	int tag = 1;
	int pubPipeError = 0;
	char *token = strtok(line, delim);

	while(1){
		printf("token: %s\n", token);
		
		if (token == NULL){
			if (pubPipeError){
				if (!tag) {
					check_pipeNtable(cli);
					decrease_pipeNtable(cli, 0);
				}
				return;
			}
			*tmp = NULL;
			if (execute(executeCMD, cli, cmd, tag) != -1 || !tag){
				decrease_pipeNtable(cli, 0);
			}
			return;
		}

		if (strcmp(token, "yell") == 0){
			char yellMsg[300] = "";
			strcpy(yellMsg, cmd->cmdBuf+(strspn(cmd->cmdBuf, "yell")+1));
			
			broadcastHandler(cli, yellMsg, 2);
			return;
		}

		if (strcmp(token, "tell") == 0){
			token = strtok(NULL, delim);
			int reciever = atoi(token) - 1;

			char tellMsg[MAX_MSG] = "";
			if (reciever < 9){ //tell ID is <10, only one digit
				strcpy(tellMsg, cmd->cmdBuf+7);
			} else { // tell ID is >=10, have two digit
				strcpy(tellMsg, cmd->cmdBuf+8);
			}
			tellHandler(cli, reciever, tellMsg);
			return;
		}
		/***
		* handle special characters
		*/
		if (strcmp(token, "|") == 0){ //general pipe
			cmd->pipeN = -1;
			exeStatus = 1;
			token = strtok(NULL, delim); //go to next token begin last token
			continue;
		}
		if (token[0] == '|' && token[1] != '\0'){ //have pipe number |N
			cmd->pipeN = atoi(token+1); //get pipe number
			exeStatus = 1;
			token = strtok(NULL, delim);
			continue;
		}
		if (token[0] == '!'){
			cmd->errPipeN = atoi(token+1); //get error pipe number
			exeStatus = 1;
			token = strtok(NULL, delim);
			continue;
		}
		if (strcmp(token, ">") == 0){
			cmd->writeFile = 1;
			exeStatus = 1;
			token = strtok(NULL, delim);
			strcpy(cmd->fileName, token); //get file name
			token = strtok(NULL, delim);
			continue;
		}
		if (token[0] == '>'){ //public pipe out
			my_lock(semIdForPubPipe);
			if ( publicPipeIndex[atoi(token+1) - 1] ){
				my_unlock(semIdForPubPipe);
				pubPipeError = 1;
				sprintf(msg, "*** Error: public pipe #%d already exists. ***\n", atoi(token+1));
				write(cli->clientFD, msg, strlen(msg));
				token = strtok(NULL, delim);
				continue;
			} else {
				publicPipeIndex[atoi(token+1) - 1] = 1;
				my_unlock(semIdForPubPipe);
				cmd->publicPipeOut = 1;
				exeStatus = 1;
				sprintf(cmd->writePublicPipe, "../%s.txt", token+1);
				token = strtok(NULL, delim);
				continue;
			}
		}
		if (token[0] == '<'){ //public pipe in
			my_lock(semIdForPubPipe);
			if (publicPipeIndex[atoi(token+1) - 1] == 0){
				my_unlock(semIdForPubPipe);
				pubPipeError = 1;
				sprintf(msg, "*** Error: public pipe #%d does not exist yet. ***\n", atoi(token+1));
				write(cli->clientFD, msg, strlen(msg));
				token = strtok(NULL, delim);
				continue;
			} else {
				publicPipeIndex[atoi(token+1) - 1] = 0;
				my_unlock(semIdForPubPipe);
				cmd->publicPipeIn = 1;
				// exeStatus = 1;
				sprintf(cmd->readPublicPipe, "../%s.txt", token+1);
				token = strtok(NULL, delim);
				continue;
			}
		}

		if (exeStatus == 1){
			if (pubPipeError){
				if (!tag) {
					check_pipeNtable(cli);
					decrease_pipeNtable(cli, 0);
				}
				return;
			}
			*tmp = NULL;
			if (execute(executeCMD, cli, cmd, tag) == -1){
				if (tag == 0){
					decrease_pipeNtable(cli, 0);
				}
				return;
			}
			// initialize
			decrease_pipeNtable(cli, 1);
			initialize_command(cmd);
			tag = 0;
			exeStatus = 0;
			tmp = executeCMD;
			strcpy(msg, "");
		}
		*(tmp++) = token;
		token = strtok(NULL, delim);
	}
}

/***
* execute command
* return 0 is continue to do
* return 1 is exit
* return -1 is empty command
*/
int execute(char **executeCMD, PrvClient *cli, Command *cmd, int tag){
	/***
	* handle special command
	*/
	if (executeCMD[0] == NULL){
		return -1;
	}
	if (strcmp(executeCMD[0], "exit") == 0){
		cmd->exitCMD = 1;
		check_pipeNtable(cli);
		return 1;
	}
	if (strcmp(executeCMD[0], "printenv") == 0){
		printenvHandler(cli, executeCMD);
		check_pipeNtable(cli);
		return 0;
	}
	if (strcmp(executeCMD[0], "setenv") == 0){
		setenv(executeCMD[1], executeCMD[2], 1);
		check_pipeNtable(cli);
		return 0;
	}

	if (strcmp(executeCMD[0], "who") == 0){
		whoHandler(cli);
		check_pipeNtable(cli);
		return 0;
	}

	if (strcmp(executeCMD[0], "name") == 0){
		nameHandler(cli, executeCMD[1]);
		check_pipeNtable(cli);
		return 0;
	}

	int pipeIndex = 0;
	int errPipeIndex = 0;
	if (cmd->pipeN != 0)
		pipeIndex = build_pipeNtable(cli->pipeNtable, cmd->pipeN);
	if (cmd->errPipeN > 0)
		errPipeIndex = build_pipeNtable(cli->pipeNtable, cmd->errPipeN);

	int childpid;
	if ((childpid = fork()) < 0){
		// Error forking
    	printf("[execute error forking]\n");
      	exit(EXIT_FAILURE);
	} else if (childpid == 0){
		// child process
		// handle pipe
		for (int i = 0; i < MAX_PIPE_N; i++){ //check if pipe number is last one
			if (cli->pipeNtable[i][0] == 0){
				dup2(cli->pipeNtable[i][2]/10000, STDIN_FILENO); //pipeout pipe to stdin
				break;
			}
		}

		if (cmd->pipeN != 0){
			dup2(abs(pipeIndex)%10000, STDOUT_FILENO); //stdout pipe to pipein
		} else {
			dup2(cli->clientFD, STDOUT_FILENO); //socket stdin
		}

		if (cmd->errPipeN > 0){
			dup2(abs(errPipeIndex)%10000, STDERR_FILENO); //stderr pipe to errpipe
		} else {
			dup2(cli->clientFD, STDERR_FILENO); //socket stderr
		}

		// > write file
		if (cmd->writeFile){
			FILE *fPtr;
			fPtr = freopen(cmd->fileName, "w", stdout);
			if(!fPtr){
	          printf("[failed to write file]\n");
	          exit(EXIT_FAILURE);
	        }
		}

		// public pipe in 
		if (cmd->publicPipeIn){
			FILE *ppr;
			ppr = freopen(cmd->readPublicPipe, "r", stdin);
			if(!ppr){
	          printf("[failed to read public pipe file]\n");
	          exit(EXIT_FAILURE);
	        } else {
	        	remove(cmd->readPublicPipe); //remove public pipe file
	        }
		}

		// public pipe out
		if (cmd->publicPipeOut){
			FILE *ppw;
			ppw = freopen(cmd->writePublicPipe, "w", stdout);
			if(!ppw){
	          printf("[failed to write public pipe file]\n");
	          exit(EXIT_FAILURE);
	        }
		}

		if (execvp(executeCMD[0], executeCMD) == -1){
			unknownCMD(cli->clientFD, executeCMD[0]);
			cmd->publicPipeOut = 0;
			cmd->publicPipeIn = 0;
			exit(EXIT_FAILURE);
		}
	} else {
		// parent process
		int status;
		printf("[wait pid: %d]\n", wait(&status));
		if (status != 256) {
			if (cmd->publicPipeIn){
				broadcastHandler(cli, cmd->cmdBuf, 3);
			}
			if (cmd->publicPipeOut){
				broadcastHandler(cli, cmd->cmdBuf, 4);
			}
		}
		if (status != 256 || !tag)
	    	check_pipeNtable(cli);

	    if (status == 256) {
	    	if (pipeIndex > 0){
	    		for (int i = 0; i < MAX_PIPE_N; i++){
	    			if (cli->pipeNtable[i][0] == pipeIndex){
	    				close(cli->pipeNtable[i][2] % 10000);
	    				close(cli->pipeNtable[i][2] / 10000);
	    				cli->pipeNtable[i][0] = -1;
	    				break;
	    			}
	    		}
	    	}
	    	if (errPipeIndex > 0){
	    		for (int j = 0; j < MAX_PIPE_N; j++){
	    			if (cli->pipeNtable[j][0] == errPipeIndex){
	    				close(cli->pipeNtable[j][2] % 10000);
	    				close(cli->pipeNtable[j][2] / 10000);
	    				cli->pipeNtable[j][0] = -1;
	    				break;
	    			}
	    		}
	    	}
	    	return -1;
	    }
	}
	return 0;
}

/***
* config  act message
* 0       new client
* 1       named
* 2       yelled
* 3       received public pipe
* 4       piped to
* 5       client left
*/
int broadcastHandler(PrvClient *client, char *broadcastMsg, int config){
	if(client->clientID != -1){
		my_lock(semIdForClient);
		char msg[300] = "";
		switch(config){
			case 0:
				// new client: no name
				sprintf(msg, "*** User '%s' entered from CGILAB/511. ***\n", shmClient[client->clientID].clientName);
				break;
			case 1:
				// named
				sprintf(msg, "*** User from CGILAB/511 is named '%s'. ***\n", shmClient[client->clientID].clientName);
				break;
			case 2:
				// yelled
				sprintf(msg, "*** %s yelled ***: %s\n",
					shmClient[client->clientID].clientName, broadcastMsg);
				break;
			case 3:
				// received public pipe
				sprintf(msg, "*** %s (#%d) just received via '%s' ***\n",
					shmClient[client->clientID].clientName, shmClient[client->clientID].clientID+1, broadcastMsg);
				break;
			case 4:
				// piped to
				sprintf(msg, "*** %s (#%d) just piped '%s' ***\n",
					shmClient[client->clientID].clientName, shmClient[client->clientID].clientID+1, broadcastMsg);
				break;
			case 5:
				// client left
				sprintf(msg, "*** User '%s' left. ***\n", shmClient[client->clientID].clientName);
				break;
		}

		// put broadcast message into chat buffer
		for (int i = 0; i < MAX_CLIENT; i++){
			if (shmClient[i].clientID != -1){
				for (int j = 0; j < MAX_URMSG; j++){
					if (strcmp(shmClient[i].chatBuf[j], "") == 0){
						strcpy(shmClient[i].chatBuf[j], msg);
						break;
					}
				}
			}
		}
		my_unlock(semIdForClient);

		int pid;
		for (int i = 0; i < MAX_CLIENT; i++){
			my_lock(semIdForClient);
			if (shmClient[i].clientID >= 0){
				pid = shmClient[i].clientPID;
			} else{
				pid = 0;
			}
			my_unlock(semIdForClient);
			if (pid > 0){
				printf("[kill: %d]\n", pid);
				kill(pid, SIGUSR1);
			}
		}
		return 1;
	}else{
		return 0;
	}
}

// print broadcast message to everyone
void write_message(int){
	int id;
	printf("[write message]\n");
	my_lock(semIdForClient);
	// get client id
	for (int i=0; i<MAX_CLIENT; i++) {
		if (shmClient[i].clientID != -1 && shmClient[i].clientPID == getpid()) {
			id = i;
			break;
		}
	}
	// write message
	for (int j=0; j<MAX_URMSG; j++) {
		if ( strcmp(shmClient[id].chatBuf[j], "") == 0)
			break;
		write(shmClient[id].clientFD, shmClient[id].chatBuf[j], strlen(shmClient[id].chatBuf[j]));
		// initialize
		strcpy(shmClient[id].chatBuf[j], "");
	}
	my_unlock(semIdForClient);
}

void remove_sharedmemory(int){
	printf("[remove shared memory]\n");
	shmctl(shmClientID,IPC_RMID,0);
	shmctl(pubPipeID,IPC_RMID,0);
	semctl(semIdForClient, 0, IPC_RMID, (struct semun *)0);
	semctl(semIdForPubPipe, 0, IPC_RMID, (struct semun *)0);
	exit(1);
}

void whoHandler(PrvClient *cli){
	write(cli->clientFD, "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n", strlen("<ID>\t<nickname>\t<IP/port>\t<indicate me>\n"));
	my_lock(semIdForClient);
	for (int i = 0; i < MAX_CLIENT; i++){
		if(shmClient[i].clientID != -1){
			char msg[100] = "";
			sprintf(msg, "%d\t%s\tCGILAB/511", i+1, shmClient[i].clientName);
			write(cli->clientFD, msg, strlen(msg));
			if (cli->clientID == i){
				write(cli->clientFD, "\t<-me\n", strlen("\t<-me\n"));
			} else {
				write(cli->clientFD, "\n", strlen("\n"));
			}
		}
	}
	my_unlock(semIdForClient);
}

void nameHandler(PrvClient *cli, char *name){
	// check exists
	my_lock(semIdForClient);
	for (int i = 0; i < MAX_CLIENT; i++){
		if (shmClient[i].clientID != -1){
			if (strcmp(name, shmClient[i].clientName) == 0){
				char msg[100] = "";
				sprintf(msg, "*** User '%s' already exists. ***\n", name);
				write(cli->clientFD, msg, strlen(msg));
				my_unlock(semIdForClient);
				return;
			}
		}
	}

	if (strlen(name) > 20){ //check if name more than 20 characters
		strncpy(shmClient[cli->clientID].clientName, name, 20);
	} else {
		strcpy(shmClient[cli->clientID].clientName, name);
	}
	my_unlock(semIdForClient);
	broadcastHandler(cli, NULL, 1);
}

void tellHandler(PrvClient *cli, int reciever, char *tellMsg){
	char msg[300] = "";

	my_lock(semIdForClient);
	if (reciever > -1 && reciever < 30){
		if (shmClient[reciever].clientID != -1){
			sprintf(msg, "*** %s told you ***: %s\n", shmClient[cli->clientID].clientName, tellMsg);
			for (int i = 0; i <MAX_URMSG ; i++){
				if (strcmp(shmClient[reciever].chatBuf[i], "") == 0){
					strcpy(shmClient[reciever].chatBuf[i], msg);
					kill(shmClient[reciever].clientPID, SIGUSR1);
					break;
				}
			}
		} else {
			sprintf(msg, "*** Error: user #%d does not exist yet. ***\n", (reciever + 1));
			write(cli->clientFD, msg, strlen(msg));
		}
	}
	my_unlock(semIdForClient);
}

void unknownCMD(int sockfd, char *CMD){
	write(sockfd, "Unknown command: [", strlen("Unknown command: ["));
	write(sockfd, CMD, strlen(CMD));
	write(sockfd, "].\n", strlen("].\n"));
}

void printenvHandler(PrvClient *cli, char **executeCMD){
	write(cli->clientFD, executeCMD[1], strlen(executeCMD[1]));
	write(cli->clientFD, "=", 1);
	write(cli->clientFD, getenv(executeCMD[1]), strlen(getenv(executeCMD[1])));
	write(cli->clientFD, "\n", 1);
}

int build_pipeNtable(int pipeNtable[MAX_PIPE_N][3], int N){
	// int pipeIndex = 0;
	int i;
	int pipe_fd[2];

	if (N > 0){
		for (int j = 0; j < MAX_PIPE_N; j++){
			if (pipeNtable[j][0] == N){
				return -1 * pipeNtable[j][2];
			}
		}
	}
	
    if (pipe(pipe_fd) == -1){
      	printf("[Error: Unable to create pipe.]\n");
      	exit(EXIT_FAILURE);
    }

    for (i = 0; i < MAX_PIPE_N; i++){
		if (pipeNtable[i][0] == -1){
			// counter
			pipeNtable[i][0] = abs(N); //pipe number
			// tag for number pipe
			if (N>0) {
				pipeNtable[i][1] = 1; //have |N
			} else {
				pipeNtable[i][1] = 0; //general pipe
			}
			pipeNtable[i][2] = 10000 * pipe_fd[0] + pipe_fd[1]; //pipe in
			// pipeNtable[i][2] = pipe_fd[0]; //pipe out
			// pipeIndex = i;
			break;
		}
    }
    printf("pipeNtable[i][0]:%d, pipeNtable[i][2]:%d\n", pipeNtable[i][0], pipeNtable[i][2]);
    return pipeNtable[i][2];
}

// decrease number-pipe counter
void decrease_pipeNtable(PrvClient *cli, int N){
  if(N == 1){
    for (int i = 0; i < MAX_PIPE_N; i++){
      if( cli->pipeNtable[i][0] == 1 && !cli->pipeNtable[i][1])
        cli->pipeNtable[i][0]--;
    }
  } else {
    for (int j = 0; j < MAX_PIPE_N; ++j){
      if( cli->pipeNtable[j][0] > 0 ){
        cli->pipeNtable[j][0]--;
      }
    }
  }
}

// check table when back to parent process
void check_pipeNtable(PrvClient *cli){
  for (int i = 0; i < MAX_PIPE_N; ++i){
    if(cli->pipeNtable[i][0] == -1)
      continue;

    if(cli->pipeNtable[i][0] == 1)
      close(cli->pipeNtable[i][2] % 10000); //close pipe fd in

    if(cli->pipeNtable[i][0] == 0){
      close(cli->pipeNtable[i][2] / 10000); //close pipe fd out
      cli->pipeNtable[i][0] = -1; //initialize
    }
  }
}

void clean_pipeNtable(PrvClient *cli){
	for (int i = 0; i < MAX_PIPE_N; i++)
	{
		if (cli->pipeNtable[i][0] != -1){
			close(cli->pipeNtable[i][2]/10000);
			close(cli->pipeNtable[i][2]%10000);
		}
	}
}

void my_lock(int semid){
	if(semop(semid, &op_lock[0], 2) < 0)
		printf("[semop lock error]\n");
}

void my_unlock(int semid){
	if(semop(semid, &op_unlock[0], 1) < 0)
		printf("[semop lock error]\n");
}

void welcome(int sockfd){
	//Print welcome message.
	write(sockfd, "****************************************\n** Welcome to the information server. **\n****************************************\n",
		strlen("****************************************\n** Welcome to the information server. **\n****************************************\n"));
}

int passiveTCP(void){
	printf("[passiveTCP]\n");
	
	int sockfd, port;
	struct sockaddr_in serv_addr;

	printf("Enter port: ");
	scanf("%d", &port);
    
    sockfd =  socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
       printf("when opening socket... \n");
	
	bzero((char *)&serv_addr, sizeof(serv_addr));
	
	serv_addr.sin_family = AF_INET;  
	serv_addr.sin_addr.s_addr = INADDR_ANY;  
    serv_addr.sin_port = htons(port);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		printf("when binding... \n");
	}
    	
    listen(sockfd,5);

    return sockfd;
}

int passivesock(char *service, char *protocol, int qlen){
	struct servent *pse;	//Pointer to service information entry.
	struct protoent *ppe;	//Pointer to protocol information entry.
	struct sockaddr_in sin;	//An Internet endpoint address.
	int s, type;			//Socket descriptor and socket type.
	u_short portbase = 0;

	bzero((char *)&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;

	//Map service name to port number.
	if (pse == getservbyname(service, protocol))
		sin.sin_port = htons(ntohs((u_short)pse->s_port) + portbase);
	else if ((sin.sin_port = htons((u_short)atoi(service))) == 0)
		err_dump("[passivesock]: can't get service entry");

	//Map protocol name to protocol number.
	if ((ppe = getprotobyname(protocol)) == 0)
		err_dump("[passivesock]: can't get protocol entry");

	//Use protocol to choose a socket type.
	if (strcmp(protocol, "udp") == 0)
		type = SOCK_DGRAM;
	else
		type = SOCK_STREAM;

	//Allocate a socket.
	s = socket(PF_INET, type, ppe->p_proto);
	if (s < 0)
		err_dump("[passivesock]: can't create socket");

	//Bind the socket.
	if (bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
		err_dump("[passivesock]: can't bind to port");

	// listen the socket
	if (type == SOCK_STREAM && listen(s, qlen) < 0)
		err_dump("[passivesock]: can't listen on port");

	return s;
}

void err_dump(const char *msg){
	perror(msg);
	exit(EXIT_FAILURE);
}