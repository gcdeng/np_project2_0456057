#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <ctype.h>
#include <netdb.h>

#define RL_BUFSIZE 15000
#define MAX_CMD_COUNT 256
#define TOK_DELIM " \0\t\r\n\a" //split command line by  \0\t\r\n\a

// process command function
void shell_loop(int);
char *read_line(int);
void modify_commandLine(char *);
char **split_line(char *);
void fetch(char *);
void analyze_command_line(int &);
int identify_unknownMsg(char *);
int execute(int, int, int, int, int, int, int, int, int, int, int);

// process pipe function
void determine_pipe_status(int &, int &);
int get_pipe_number(void);
int create_pipe(int, int &, int &);
int nextPipeExist(int, int &, int &);
int pipeStdinExist(int &, int &);
void initial_pipeNtable(void);
void check_pipeNtable(void);
void update_pipeNtable(int,int, int);
void decreasing_pipeNtable(int);

// env call function
int identify_envcall(char *);
void env_execute(int, int);

// server function
void writeToClient(int, char *);
int server(int);
int server_accept(int);
void server_close(int);

// global variables
char line[RL_BUFSIZE];
int lineIndex;
char *command[30];
int commandPosition;
char *mainCommand;
char *file;
int pipeNtable[1000][3]; // pipeNtable[N][0]: counter, pipeNtable[N][1]: pipe_fd_in (4), pipeNtable[N][2]: pipe_fd_out (3)

int main(int argc, char **argv, char **envp)
{
  // create socket
  int port = 4475;
  if (argv[1] != NULL){
    port = atoi(argv[1]);
  }
  int fd = server(port);
  int newfd, childServer, status, next = 1;
  do{
    // preset setenv path
    chdir("./ras");
    setenv("PATH", "bin:.", 1);

    // socket accept
    while (newfd = server_accept(fd)){

      childServer = fork();
      if (childServer < 0) {
        // Error forking
        printf("[server forking error]\n");
        exit(EXIT_FAILURE);
      } else if (childServer == 0){
        // Child process

        // welcome
        char WELCOME_MESSAGE[] = "****************************************\n** Welcome to the information server. **\n****************************************\n";
        writeToClient(newfd, WELCOME_MESSAGE);

        // shell
        shell_loop(newfd);

      } else {
        // parent process

        server_close(newfd);// close socket
        // wait(&status); //wait for child
      }
    }
  } while(1);
  return 0;
}

/*** loop of a shell
 * Read command line
 * Parse command
 * Process pipe
 * Execute
 */
void shell_loop(int newfd){
  int status, writeFile = 0, envCommand = 0;
  int stdoutPipe = 0, pipeStdin = 0, stderrPipe = 0; //determine status of pipe
  int pipeIn, pipeOut, lastPipeIn, lastPipeOut, stderrPipeIn, stderrPipeOut;

  initial_pipeNtable();

  do{
    writeToClient(newfd, "% ");

    // read line
    modify_commandLine(read_line(newfd));
    // printf("read line:%s, length:%d\n", line, strlen(line));

    while(line[lineIndex] != '\0'){
      analyze_command_line(writeFile);
      determine_pipe_status(stdoutPipe, stderrPipe);

      printf("stdoutPipe: %d, pipeStdin: %d, stderrPipe: %d\n", stdoutPipe, pipeStdin, stderrPipe);

      // exit command
      if (strcmp("exit", mainCommand) == 0){
        status = 0; //exit, server accept again
        break;
      }

      // do env call: setenv or printenv
      envCommand = identify_envcall(mainCommand);
      if(envCommand > 0){
        env_execute(envCommand, newfd);
        status = 1; //cotinue
        break;
      }

      // identify unknown message
      if ( 0 == identify_unknownMsg(mainCommand)){
        writeToClient(newfd, "Unknown command: [");
        writeToClient(newfd, mainCommand);
        writeToClient(newfd, "].\n");
        status = 1; //cotinue
        break;
      }

      create_pipe(stdoutPipe, pipeIn, pipeOut);
      create_pipe(stderrPipe, stderrPipeIn, stderrPipeOut);

      status = execute(newfd, stdoutPipe, pipeStdin, stderrPipe,
                      pipeIn, pipeOut,
                      lastPipeIn, lastPipeOut,
                      stderrPipeIn, stderrPipeOut,
                      writeFile);

      pipeStdin = pipeStdinExist(lastPipeIn, lastPipeOut);
      if ( identify_unknownMsg(mainCommand) != 0 ) //not unknown
        decreasing_pipeNtable(1);

    }
    // if ( (identify_unknownMsg(mainCommand) != 0) ) //do not have unknown
    decreasing_pipeNtable(2);

    // initialize
    writeFile = 0;
    envCommand = 0;
  } while(status);
}

/***
* process command
*
*/
/*reading a line*/
char *read_line(int newfd){
  int n = -1;
  int end = 0;
  char buffer[RL_BUFSIZE];
  char *inputCommand = new char[RL_BUFSIZE];

  bzero(inputCommand, 15000);
  while(!end){
    bzero(buffer, 15000);
    if ((n = read(newfd, buffer, 15000)) < 0){
      printf("reading from socket...\n");
    } else {
      strcat(inputCommand, buffer);
      if(buffer[n-1] == '\n'){
        end = 1;
      }
    }
  }
  return inputCommand;
}

// modify input command line to line[] global variable, delete \r, modify space
void modify_commandLine(char * inputCommand){
  int space=0, index=0;
  lineIndex=0;

  while(inputCommand[index] != '\n'){
    if (inputCommand[index] == '\r'){
      index++;
      continue;
    } else if (inputCommand[index] != ' '){
      line[lineIndex++] = inputCommand[index++];
      space = 0;
    } else {
      index++;
      if (lineIndex==0)
        continue;
      if (space == 0){
        line[lineIndex++] = ' ';
        space = 1;
      }
    }
  }
  if (line[lineIndex-1] == ' ')
    line[lineIndex-1] = '\0';
  else
    line[lineIndex] = '\0'; //end char tag

  // initialize
  lineIndex=0;
}

/*Parsing the line*/
char **split_line(char *line){
  int bufsize = MAX_CMD_COUNT;
  int position = 0;
  char **tokens = (char **)malloc(bufsize * sizeof(char *));
  char *token;

  if (!tokens) {
    fprintf(stderr, "allocation error\n");
    exit(EXIT_FAILURE);
  }

  // split string
  token = strtok(line, TOK_DELIM);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    // If we have exceeded the buffer, reallocate.
    if (position >= bufsize) {
      bufsize += MAX_CMD_COUNT;
      tokens = (char **)realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
        fprintf(stderr, "allocation error\n");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, TOK_DELIM);
  }
  tokens[position] = NULL; //NULL end
  return tokens;
}

// fetch specific command
void fetch(char *tmpCommand){
  int index =0;
  while(line[lineIndex] != '\0' && line[lineIndex] != ' '){
    tmpCommand[index] = line[lineIndex];

    index++;
    lineIndex++;
  }
  tmpCommand[index] = '\0';
}

// get command
void analyze_command_line(int & writeFile){
  char tmpCommand[30];
  // initialize command string array
  for (int i = 0; i < commandPosition; ++i)
  {
    free(command[i]);
  }

  commandPosition=0;
  writeFile=0;
  while(line[lineIndex] != '|' && line[lineIndex] != '!' && line[lineIndex] != '\0'){
    if (line[lineIndex] == ' ')
    {
      lineIndex++;
      continue;
    }

    if (line[lineIndex] == '>')
    {
      writeFile = 1;//need to execute write file

      free(file);
      lineIndex += 2; //go to file title index
      fetch(tmpCommand);
      file = (char *)malloc(strlen(tmpCommand) * sizeof(char *));
      strcpy(file, tmpCommand);
      // printf("%s\n", file);
      break;
    }

    fetch(tmpCommand);
    command[commandPosition] = (char *)malloc(strlen(tmpCommand) * sizeof(char *));
    strcpy(command[commandPosition], tmpCommand);
    commandPosition++;
  }
  command[commandPosition] = NULL;

  // get main command which need to execute
  mainCommand = command[0];
}

// identify unknown command
int identify_unknownMsg(char *command){
  if ( strcmp("ls", command) == 0) return 1;
  if ( strcmp("cat", command) == 0 ) return 1;
  if ( strcmp("noop", command) == 0 ) return 1;
  if ( strcmp("number", command) == 0 ) return 1;
  if ( strcmp("removetag", command) == 0 ) return 1;
  if ( strcmp("removetag0", command) == 0 ) return 1;
  if ( strcmp("printenv", command) == 0 ) return 1;
  if ( strcmp("setenv", command) == 0 ) return 1;

  return 0;
}

/*execute command*/
int execute(int newfd, int stdoutPipe, int pipeStdin, int stderrPipe, int pipeIn, int pipeOut,
            int lastPipeIn, int lastPipeOut, int stderrPipeIn, int stderrPipeOut, int writeFile){

  if (mainCommand == NULL) {
    // An empty command was entered.
    return 1;
  } else {
    pid_t childpid;
    int status;

    // fork children process
    childpid = fork();

    if (childpid < 0) {
      // Error forking
      printf("[error forking]\n");
      exit(EXIT_FAILURE);
    } else if (childpid == 0){
      // Child process
      if(stdoutPipe)
      {
        dup2(pipeIn, STDOUT_FILENO); //4->1
      }
      else{ //socket fd stdin
        dup2(4, 1);
      }

      if(pipeStdin)
      {
        dup2(lastPipeOut, STDIN_FILENO); //3->0
      }

      if(stderrPipe)
      {
        dup2(stderrPipeIn, 2);
      }
      else{// socket stderr
        dup2(4, 2);
      }

      // if > write file
      if(writeFile){
        FILE *fPtr;
        fPtr = freopen(file, "w", stdout);
        if(!fPtr){
          printf("file write failed\n");
          exit(1);
        }
      }

      // printf("%s, %d", mainCommand ,strlen(mainCommand));

      // execute command
      if (execvp(mainCommand, command) == -1){
        // error execute, occur unknown message in wrong PATH
        writeToClient(newfd, "Unknown command: [");
        writeToClient(newfd, mainCommand);
        writeToClient(newfd, "].\n");
        exit(EXIT_FAILURE);
      }

    }
    else {
      // Parent process
      check_pipeNtable();

      // wait for child.
      wait(&status);
    }
  }

  printf("[Info] \"%s\" has completed.\n", mainCommand);
  return 1;
}


/***
* process pipe
*
**/
// create pipe
// determine pipe status
void determine_pipe_status(int & stdoutPipe, int & stderrPipe){
  stdoutPipe = 0;
  stderrPipe = 0;
  if (line[lineIndex] == '|')
  {
    switch(line[++lineIndex]){
      case ' ': //if pipe to next instruction
        stdoutPipe = 1;
        if(line[lineIndex+1] == '!'){
          lineIndex+=2; //go to !N number index
          stderrPipe = get_pipe_number();
        }
        break;
      case '\0': //if have not char after pipe
        stdoutPipe = 1;
        break;
      default: // if have pipe number after pipe
        stdoutPipe = get_pipe_number();
        if (line[lineIndex] == ' ') //if have ! after |
        {
          lineIndex += 2; //go to !N number index
          stderrPipe = get_pipe_number(); //get ! number
        }
        break;
    }
  }

  if (line[lineIndex] == '!')
  {
    lineIndex++;
    stderrPipe = get_pipe_number(); //get ! number
    if (line[lineIndex] == ' ') //if have | after !
    {
      lineIndex+=2; //go to |N number index
      stdoutPipe = get_pipe_number();
    }
  }
}

int create_pipe(int havePipe, int & pipeIn, int & pipeOut){
  if(havePipe < 1)
    return 0;

  if( (nextPipeExist(havePipe, pipeIn, pipeOut)) == 0 ){//if pipe is not exist
    int pipes_fd[2];
    if (pipe(pipes_fd) == -1){
      fprintf(stderr, "Error: Unable to create pipe.\n");
      exit(EXIT_FAILURE);
    }
    pipeIn = pipes_fd[1]; // pipe fd in (4)
    pipeOut = pipes_fd[0]; // pipe fd out (3)
    update_pipeNtable(havePipe, pipeIn, pipeOut);
  }
  return 1;
}

// if next pipe is exist
int nextPipeExist(int havePipe, int & pipeIn, int & pipeOut){
  for (int i = 0; i < 1000; ++i){
    if(pipeNtable[i][0] == -1)
      continue;
    if(pipeNtable[i][0] == havePipe){
      pipeIn = pipeNtable[i][1];
      pipeOut = pipeNtable[i][2];
      return 1;
    }
  }
  return 0;
}

// determine if or not have to pipe to stdin, update last pipe
int pipeStdinExist(int & lastPipeIn, int & lastPipeOut){
  for (int i = 0; i < 1000; ++i)
  {
    if( pipeNtable[i][0] == 1){ //counter = 1, indicated last command line
      lastPipeIn = pipeNtable[i][1];
      lastPipeOut = pipeNtable[i][2];
      return 1;
    }
  }
  return 0;
}

/***
* process numbered-pipe table
*
*/
// initialize table
void initial_pipeNtable(void){
  for (int i = 0; i < 1000; i++)
  {
    pipeNtable[i][0] = -1;
  }
}
// check table when back to parent process
void check_pipeNtable(void){
  for (int i = 0; i < 1000; ++i)
  {
    if(pipeNtable[i][0] == -1)
      continue;

    if(pipeNtable[i][0] == 1)
      close(pipeNtable[i][1]); //close pipe fd in

    if(pipeNtable[i][0] == 0){
      close(pipeNtable[i][2]); //close pipe fd out
      pipeNtable[i][0] = -1; //initialize
    }
  }
}

// return number of numbered-pipe
int get_pipe_number(void){
  int pipeNumber = 0;
  while(isdigit(line[lineIndex])){
    pipeNumber = 10 * pipeNumber + (line[lineIndex] - '0');
    lineIndex++;
  }
  if(pipeNumber == 0)
    return 1;

  return pipeNumber;
}

// update table
void update_pipeNtable(int havePipe,int pipeIn, int pipeOut){
    int i = 0;
    while(pipeNtable[i][0] != -1)
      i++;

    pipeNtable[i][0] = havePipe;
    pipeNtable[i][1] = pipeIn;
    pipeNtable[i][2] = pipeOut;
}

// decrease number-pipe counter
void decreasing_pipeNtable(int N){
  if(N==1){
    for (int i = 0; i < 1000; ++i){
      if( pipeNtable[i][0] == 1)
        pipeNtable[i][0]--;
    }
  } else {
    for (int j = 0; j < 1000; ++j)
    {
      if( pipeNtable[j][0] > 1 ){
        pipeNtable[j][0]--;
      }
    }
  }
}

/***
*process Environment variables
*
*/
// determine which env call
int identify_envcall(char * mainCommand){
  if(strcmp("setenv", mainCommand) == 0)
    return 1;
  if(strcmp("printenv", mainCommand) == 0)
    return 2;

  return 0;
}
// execute env system call
void env_execute(int envCommand, int newfd){
  switch(envCommand){
    case 1:
      setenv(command[1], command[2], 1);
      break;
    case 2:
      writeToClient(newfd, "PATH=");
      writeToClient(newfd, getenv(command[1]));
      writeToClient(newfd, "\n");
      break;
  }
}

/***
* TCP server socket connection
* socket server function
*/
// create socket, bind, listen
int server(int port){
  int sockfd;
  struct sockaddr_in serv_addr;

  sockfd =  socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
     printf("when opening socket... \n");

  bzero ((char *) &serv_addr, sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);

  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
    printf("when binding... \n");
  }

  listen(sockfd,5);

  return sockfd;
}
// accept client connection
int server_accept(int sockfd){
  struct sockaddr_in cli_addr;
  socklen_t clilen;
  clilen = sizeof(cli_addr);

  int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

  // show client IP
  printf("%d.%d.%d.%d\n",
  int(cli_addr.sin_addr.s_addr&0xFF),
  int((cli_addr.sin_addr.s_addr&0xFF00)>>8),
  int((cli_addr.sin_addr.s_addr&0xFF0000)>>16),
  int((cli_addr.sin_addr.s_addr&0xFF000000)>>24));

  if (newsockfd < 0)
      printf("accepting...\n");

  // close(sockfd);

  return newsockfd;
}

// write message
void writeToClient(int newfd, char * msg)
{
  if(write(newfd, msg, strlen(msg) )<0){
    printf("writing to socket... \n");
  }
}

//close socket
void server_close(int sockfd){
  close(sockfd);
}
